import React, { Component } from 'react';
import { Stage, Layer, Image } from 'react-konva';
import Konva from 'konva';
import { Scrollbars } from 'react-custom-scrollbars';

import Thumbnail from './explorer/Thumbnail';
import Tabs from './explorer/Tabs';

import styles from './styles/Explorer.css';
import stylesWorkspace from './styles/Workspace.css';

export default class Explorer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [],
      explorerState: null
      
    };
    this.loadAll();
  }
 // .context('../img/frag', false, /\.(png|jpe?g|svg)$/)
      // .keys();
  loadAll() {
    this.state.images = require
      .context('../../resources/images/', false, /\.(png|jpe?g|svg)$/)
      .keys()
      .filter(element => {
        return element.includes('r');
      });
  }

  displayAll() {
    var rows1 = [];
    var rows2 = [];

    this.state.images.forEach((element, index) => {
      if (index % 2 === 0)
        rows1.push(
          <Thumbnail
            key={element}
            element={element}
            index={index}
            images={this.props.fragmentsList}
          />
        );
      else
        rows2.push(
          <Thumbnail
            key={index}
            element={element}
            index={index}
            images={this.props.fragmentsList}
          />
        );
    });
    return (
      <div className={styles.thumbnailList}>
        <div className={styles.fluidContainer}>
          <div className={styles.columnOne}>{rows1}</div>
          <div className={styles.columnTwo}>{rows2}</div>
        </div>
      </div>
    );
  }

  displayAllHints() {
    var rows1 = [];
    var rows2 = [];

    this.props.hints
      .filter(element => {
        return element.includes('r');
      })
      .forEach((element, index) => {
        if (index % 2 === 0)
          rows1.push(
            <Thumbnail
              key={index + '_hints'}
              element={'./' + element + '.png'}
              index={index}
              images={this.props.fragmentsList}
            />
          );
        else
          rows2.push(
            <Thumbnail
              key={index + '_hints'}
              element={'./' + element + '.png'}
              index={index}
              images={this.props.fragmentsList}
            />
          );
      });
    return (
      <div className={styles.thumbnailList}>
        <div className={styles.fluidContainer}>
          <div className={styles.columnOne}>{rows1}</div>
          <div className={styles.columnTwo}>{rows2}</div>
        </div>
      </div>
    );
  }

  componentDidMount() {
    this.state.explorerState = this.props.explorerState.isExplorerExtended;
    
  }
  componentDidUpdate() {
    // console.log("local Explorer State")
    // console.log(this.state.explorerState)
    // console.log("redux Explorer State")
    // console.log(this.props.explorerState.isExplorerExtended)


    if(this.state.explorerState != this.props.explorerState.isExplorerExtended) {
      this.state.explorerState = this.props.explorerState.isExplorerExtended;
      if(document.getElementById('explorer').className == styles.explorerExtended) {
        document.getElementById('explorer').className = styles.explorerWrapped;
        document.getElementById('btnWrapExplorer').className = styles.btnExplorerWrapped;
        // console.log("Now wrapped")
      } else if(document.getElementById('explorer').className == styles.explorerWrapped){
        document.getElementById('explorer').className = styles.explorerExtended;
        document.getElementById('btnWrapExplorer').className = styles.btnExplorerExtended;
        // console.log("Now extended")
      } 
    }
    console.log("EXPLORER updated")
    
    // document.getElementById('explorer').className = styles.backgroundWhite
  }
  render() {
    return (
      <div>
        

        <div className={styles.tools, styles.explorerExtended} id="explorer">
        <Tabs parent={this}>
          <div
            label="All"
            style={{
              height: '100vh',
              overflowY: 'scroll',
              overflowX: 'hidden'
            }}
          >
            {this.displayAll()}
          </div>
          <div label="Possibilities">{this.displayAllHints()}</div>
        </Tabs>
      </div>

      </div>
     
      
    );
  }
}
