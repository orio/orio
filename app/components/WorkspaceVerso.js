import React from 'react';
import { Stage, Layer, Transformer } from 'react-konva';
import Handler from './workspace/Handler.js';
import Fragment from './workspace/Fragment';
import styles from './styles/Workspace.css';
import Explorer from './Explorer';

export default class Workspace extends React.Component {

  state = {
    stage: null,
    selectedShapeName: '',
    images: []
  };

  componentDidMount() {
    this.setState({ stage: this.stageRef.getStage(),
      layer: this.layerRef.getLayer() });
  }

  componentDidUpdate() {

    if(this.props.mainState.stagePosition.x == this.state.stage.absolutePosition().x && this.props.mainState.stagePosition.y == this.state.stage.absolutePosition().y) {
      this.state.layer.getChildren(function(node){
        return node.getClassName() === 'Image';
    }).forEach(image => {
      
      
      let fragmentRedux;
      if(this.hasOpposite(image.attrs.name)){
        fragmentRedux = this.props.fragmentsList.find(fragment => fragment.src === this.getOpposite(image.attrs.name));
      }else{
          fragmentRedux = this.props.fragmentsList.find(fragment => fragment.src === image.attrs.name);
          image.scale({x:-1, y:1});
          image.opacity(0.2);
      }
      // console.log("Fragment Local");
      // console.log(image.position().x);
      // console.log("Fragment Redux");
      // console.log(fragmentRedux.position.x);
      // console.log("Stage max width")
      // console.log(this.state.stage.width())
      // (image.position().x - (fragmentRedux.position.x - image.position().x))
      let newX = this.state.stage.width() - fragmentRedux.position.x
      image.position({x: newX, y:fragmentRedux.position.y});
      image.rotation(fragmentRedux.rotation);

    });
   }
    this.state.stage.absolutePosition({x:(this.props.mainState.stagePosition.x ), y: this.props.mainState.stagePosition.y});
    this.state.stage.scale({x: this.props.mainState.stagePosition.scaleX , y:this.props.mainState.stagePosition.scaleY});
    console.log("VERSO updated")
  }

  handleStageClick = e => {
    this.setState({ selectedShapeName: e.target.name() });
    // console.log("Verso click");
    // console.log(e.target.getStage().getPointerPosition());
  };

  handleWheel = e => {
    e.evt.preventDefault();
    

    const scaleBy = 1.05;
    const stage = e.target.getStage();
    const oldScale = stage.scaleX();

    const mousePointTo = {
      x: stage.getPointerPosition().x / oldScale - stage.x() / oldScale,
      y: stage.getPointerPosition().y / oldScale - stage.y() / oldScale
    };

    let newScale = e.evt.deltaY > 0 ? oldScale * scaleBy : oldScale / scaleBy;

    newScale = newScale < 0.15 ? 0.15 : newScale;

    stage.scale({ x: newScale, y: newScale });
    this.setState({
      stageScale: newScale,
      stageX:
        -(mousePointTo.x - stage.getPointerPosition().x / newScale) * newScale,
      stageY:
        -(mousePointTo.y - stage.getPointerPosition().y / newScale) * newScale
    });
    var newPos = {
      x: -(mousePointTo.x - stage.getPointerPosition().x / newScale) * newScale,
      y: -(mousePointTo.y - stage.getPointerPosition().y / newScale) * newScale
    };
    stage.position(newPos);
    stage.batchDraw();
  };

  updateStage = e => {
    e.evt.preventDefault();
  }


  hasOpposite(imageSource){
    let found = this.props.fragmentsList.find(fragment => fragment.src === this.getOpposite(imageSource));
    return found != undefined;
  }


  getOpposite(imageSource){
    let arrayUrl = imageSource.split('/');
    let source = arrayUrl[arrayUrl.length-1].split('.')[0];
    if(source.includes('r')){
      let opposite = source.replace('r', 'v');
      return imageSource.replace(source, opposite)
    }
    let opposite = source.replace('v', 'r');
    return imageSource.replace(source, opposite);
  }

  render() {
    const availableWindowWidth = this.props.explorerState.availableWindowWidth;
    return (
      <div>
        <div
          className={styles.layerVerso}
          data-tclass="content"
          onDrop={e => {
            let stage = this.state.stage;
            stage.setPointersPositions(e);
            this.props.addFragment({
              src: e.dataTransfer.getData('src'),
              position: stage.getPointerPosition()
            });
            this.forceUpdate();
          }}
          onDragOver={e => e.preventDefault()}
        >
          <Stage
            ref={ref => {
              this.stageRef = ref;
            }}
            name="stage"
            // className={styles.stageWrapped}
            data-tclass="canvas"
            onPointerOut={this.updateStage}
            onClick={this.handleStageClick}
            width={(window.innerWidth * availableWindowWidth)/2} // remplacer 0.779 par une valeur du state, qui vient de redux
            height={window.innerHeight}
          >
            <Layer ref={ref => {
              this.layerRef = ref;
            }}
            id="layerVerso"
            className={styles.layerVerso}        
            name="layer"

            >
            {this.props.fragmentsList.map((image, index) => {
                return (
                  <Fragment
                    key={index}
                    src={image.src}
                    position={image.position}
                    opacity={1}
                    scaleX={1}
                  />
                );
              })}
              <Handler selectedShapeName={this.state.selectedShapeName} />
            </Layer>
          </Stage>
        </div>
      </div>
    );
  }
}
