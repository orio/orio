import React, { Component } from 'react';
import Explorer from './Explorer';
import Workspace from './Workspace';
import WorkspaceVerso from './WorkspaceVerso';
import Taskbar from './Taskbar'
import styles from './styles/Work.css';


type Props = {
  // addFragment: value => string,
  // synchronizePosition: value => void,
  // removeFragment: index => void,
  // displayHints: value => array,
  mainState: Object,
  explorerState: Object
};

export default class Work extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mainState: Object,
      explorerState: Object
    };
  }

  componentDidMount() {
    this.setState({
      mainState: this.props.mainState,
      explorerState: this.props.explorerState
    });        
  }

  updateComponent() {
    console.log("Work")
    console.log(this.state.explorerState)
    console.log(this.props.explorerState)
    this.forceUpdate();
  }

  render() {
    const changeTab = this.props.fragmentsActions.changeTab;
    const addFragment = this.props.fragmentsActions.addFragment;
    const removeFragment = this.props.fragmentsActions.removeFragment;
    const displayHints = this.props.fragmentsActions.displayHints;
    const synchronizePosition = this.props.fragmentsActions.synchronizePosition;
    const mainState = this.props.mainState;
    const explorerState = this.props.explorerState;
    const wrapExplorer = this.props.explorerActions.wrapExplorer;
    const extendExplorer = this.props.explorerActions.extendExplorer;
    return (


        <div className={styles.home}
          onMouseUp={this.updateComponent.bind(this)}
          onWheel={this.updateComponent.bind(this)}
          onDrop={this.updateComponent.bind(this)}
          >
            <Taskbar
              wrapExplorer={(value) => wrapExplorer(value)}
              extendExplorer={(value) => extendExplorer(value)}
              explorerState={explorerState}
            />


            <Explorer 
                  fragmentsList={mainState.fragmentsRecto} 
                  hints={mainState.fragmentsHints}
                  explorerState={explorerState}
            />
      <Workspace
              mainState={mainState}
              addFragment={(value) => addFragment(value)}
              displayHints={(hintsList) => displayHints(hintsList)}
              synchronizePosition={(value) => synchronizePosition(value)}
              removeFragment={(fragmentsRecto) => removeFragment(fragmentsRecto)}
              mainState={mainState}
              fragmentsList={mainState.fragmentsRecto}
              wrapExplorer={(value) => wrapExplorer(value)}
              extendExplorer={(value) => extendExplorer(value)}
              explorerState={explorerState}
            />
             <WorkspaceVerso
              mainState={mainState}
              fragmentsList={mainState.fragmentsVerso}
              explorerState={explorerState}
            />

            
        </div>
      // <div
      //   className={styles.home}
      //   onMouseUp={this.displayPositions.bind(this)}
      //   onWheel={this.displayPositions.bind(this)}
      //   onDrop={this.displayPositions.bind(this)}
      // >
      //   <Explorer
      //     fragmentsList={mainState.fragmentsRecto}
      //     hints={mainState.fragmentsHints}
      //   />

      //   <Workspace
      //     mainState={mainState}
      //     addFragment={value => addFragment(value)}
      //     removeFragment={fragment => removeFragment(fragment)}
      //     displayHints={hintsList => displayHints(hintsList)}
      //     fragmentsList={mainState.fragments}
      //     synchronizePosition={value => synchronizePosition(value)}
      //     removeFragment={fragmentsRecto => removeFragment(fragmentsRecto)}
      //     fragmentsList={mainState.fragmentsRecto}
      //   />
      // </div>
    



);
  }
}



