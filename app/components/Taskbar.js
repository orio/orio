import React, { Component } from 'react';
import { Stage, Layer, Image } from 'react-konva';
import Konva from 'konva';


import styles from './styles/Explorer.css';
import stylesWorkspace from './styles/Workspace.css';

export default class Explorer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      explorerState: null
      
    };
  }



  componentDidMount() {
    this.changeExplorerState = this.changeExplorerState.bind(this);

    this.state.explorerState = this.props.explorerState.isExplorerExtended;
    
  }
  componentDidUpdate() {
  
  }

  changeExplorerState() {
    
    console.log("**********************************")
    if(this.props.explorerState.isExplorerExtended == true) {
      
      this.props.wrapExplorer({
        isExplorerExtended: false,
        availableWindowWidth: 1
      });  
      
    } else {
      
      this.props.extendExplorer({
        isExplorerExtended: true,
        availableWindowWidth: 0.78
      });
    }
    // console.log("**********************************")

    this.forceUpdate();
    
  }
  render() {
    return (
      <div>
        
        <button id="btnWrapExplorer"
                onMouseUp={this.changeExplorerState}>Cacher explorer</button>
      </div>
     
      
    );
  }
}
