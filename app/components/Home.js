// @flow
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import routes from '../constants/routes.json';
import styles from './styles/Home.css';

export default class Home extends Component {
  render() {
    return (
      <div className={styles.container}>
        <img src="../app/img/logo-color.png"/>
        <h2>ORIO</h2>
        <Link className={styles.btn} to={routes.WORKPAGE}>Open project</Link>
      </div>
    );
  }
}
