import { remote } from 'electron';
const { Menu, MenuItem } = remote;

import { removeFragment } from '../../actions/fragments';

export default class MenuItems {

    constructor(cm) {
        this.contextMenu = cm;
    }

    deleteFragment(){
        // the context menu
        const contextMenu = this.contextMenu;

        return new MenuItem({
            label: 'Delete Fragment',
            click() {
                contextMenu.removeFragment();
            }
        });
    }

    moveUpFragment(){
        // the context menu
        const contextMenu = this.contextMenu;

        return new MenuItem({
            label: 'Move up',
            click() {
                contextMenu.moveUpFragment();
            }
        });
    }

    showPossibilities() {
      // the context menu
      const contextMenu = this.contextMenu;
  
      return new MenuItem({
        label: 'Show possibilities',
        click() {
          contextMenu.displayHints();
        }
      });
    }

    moveDownFragment(){
        // the context menu
        const contextMenu = this.contextMenu;

        return new MenuItem({
            label: 'Move down',
            click() {
                contextMenu.moveDownFragment();
            }
        });
    }

    getMenuItems() {
        let menuItems = new Map();

        menuItems.set("DELETE_FRAGMENT", this.deleteFragment());
        menuItems.set("MOVE_UP_FRAGMENT", this.moveUpFragment());
        menuItems.set("MOVE_DOWN_FRAGMENT", this.moveDownFragment());
        menuItems.set("SHOW_POSSIBILITIES", this.showPossibilities());

        return menuItems;
    }
}
