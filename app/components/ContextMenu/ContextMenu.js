import React from 'react';
import MenuItems from '../ContextMenu/MenuItems';
import { remote } from 'electron';
import { removeFragment } from '../../actions/fragments';
import styles from '../explorer/styles/Tab.css';
const { Menu, MenuItem } = remote;

export default class ContextMenu {
  constructor(e, fragment) {
    this.element = e;
    this.fragment = fragment;
  }

  handleContextMenu = () => {
    // do not show native context
    this.element.evt.preventDefault();

    if (this.element.target === this.element.target.getStage()) {
      // if we are on empty place of the stage we will do nothing
      return;
    }

    // Create new context menu
    const menu = new Menu();
    const cm = this;
    const mi = new MenuItems(cm, this.fragment);

    mi.getMenuItems().forEach(e => {
      menu.append(e);
    });

    menu.popup(remote.getCurrentWindow());
  };

  removeFragment() {
    this.fragment.removeFragment();
  }

  moveUpFragment() {
    this.fragment.moveUpFragment();
  }

  displayHints() {
    let arrayUrl = this.fragment.props.src.split('/');
    let name = arrayUrl[arrayUrl.length-1].split('.')[0];
    document.getElementById("tab_Possibilities").className = styles.news;
    var firstProp;
    fetch('http://185.212.226.115/papyrus_dawin/?fragment_id=' + name)
      .then(response => response.json())
      .then(data => {
        // second parse to json
        data = JSON.parse(data);

        // get first object in json
        for (var key in data) {
          if (data.hasOwnProperty(key)) {
            firstProp = data[key];
            break;
          }
        }
        this.fragment.displayHints(firstProp);
      });
  }

  moveDownFragment() {
    this.fragment.moveDownFragment();
  } 
}


