import React from 'react';
import { Transformer } from 'react-konva';
import Fragment from './Fragment';
import Link from './Link';

export default class Handler extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    firstNode: null,
  };

  componentDidMount() {
    this.checkNode();
  }
  componentDidUpdate() {
    this.checkNode();
  }

  checkNode() {
    const stage = this.transformer.getStage();
    const selectedNode = stage.findOne("." + this.props.selectedShapeName);

    if (selectedNode === this.transformer.node()) {
      return;
    }

    if (selectedNode) {
      this.transformer.attachTo(selectedNode);
      this.setState({firstNode: selectedNode});
    } else {
      this.transformer.detach();
    }

    let e = window.event;

    if(selectedNode && this.state.firstNode != null && this.state.firstNode !== selectedNode && e.shiftKey) {
      let layer = stage.children[0];
      layer.add(new Link(this.state.firstNode, selectedNode, layer).line);
    }
    
    this.transformer.getLayer().batchDraw();
  }

  render() {
    return (
      <Transformer
        name="transformer"
        resizeEnabled={false}
        rotationSnaps= {[0, 90, 180, 270]}
        ref={node => {
          this.transformer = node;
        }}
      />
    );
  }
}