import React from 'react';
import Handler from './Handler';
import { Stage, Layer, Image, Transformer } from 'react-konva';
import Konva from 'konva';

import ContextMenu from '../ContextMenu/ContextMenu';

import constants from '../../../resources/data/constants.json';
import dataDensities from '../../../resources/data/densities.json';
import styles from './styles/Fragment.css';

export default class Fragment extends React.Component {
  constructor(props) {
    super(props);

    this.state.x = this.props.position.x;
    this.state.y = this.props.position.y;
    this.state.fragment = this;
  }

  state = {
    image: null,
    x: '',
    y: '',
    fragment: null
  };

  componentDidMount() {
    this.loadImage();
  }

  componentDidUpdate(oldProps) {
    if (oldProps.src !== this.props.src) {
      this.loadImage();
    }
  }

  componentWillUnmount() {
    this.image.removeEventListener('load', this.handleLoad);
  }

  toScale(image) {
    //split src img
    const tabSrc = image.src.split('/');
    //get img file -> split num img
    const nameImg = tabSrc[tabSrc.length - 1].split('_');
    nameImg.splice(length - 1, 1);
    //assemble img familly name
    const famillyImg = nameImg.join('_');

    if (dataDensities[famillyImg] != null) {
      image.width =
        (image.width / dataDensities[famillyImg]) * constants['scaleFactor'];
      image.height =
        (image.height / dataDensities[famillyImg]) * constants['scaleFactor'];
    } else {
      image.width =
        (image.width / constants['defaultDensity']) * constants['scaleFactor'];
      image.height =
        (image.height / constants['defaultDensity']) * constants['scaleFactor'];
    }
  }

  loadImage() {
    // save to "this" to remove "load" handler on unmount
    this.image = new window.Image();
    this.image.src = this.props.src;

    this.toScale(this.image);

    this.image.addEventListener('load', this.handleLoad);
  }

  handleLoad = () => {
    // after setState react-konva will update canvas and redraw the layer
    // because "image" property is changed
    this.setState({
      image: this.image
    });
    // set origin of a fragment on its center
    this.imageNode.offsetX(this.imageNode.width() / 2);
    this.imageNode.offsetY(this.imageNode.height() / 2);

    // play animation
    this.startAnimation(this.imageNode, () => {
      // trigger cache after animation

      // cache the image
      this.imageNode.cache({ pixelRatio: 15 });
      // make a more precise image hit region
      this.imageNode.drawHitFromCache();
    });
  };

  removeFragment() {
    this.props.removeFragment(this);
  }

  moveUpFragment() {
    // Move Up function
    this.imageNode.moveUp();
    // update layer
    this.imageNode.getStage().batchDraw();
  }

  displayHints(hintsList) {
    this.props.displayHints(hintsList);
  }
  
  moveDownFragment() {
    // Move Down function
    this.imageNode.moveDown();
    // update layer
    this.imageNode.getStage().batchDraw();
  }

  startAnimation(object, callback) {
    var amplitude = 0.1;
    var period = 1000;

    let anim = new Konva.Animation(function(frame) {
      object.shadowOpacity(
        -amplitude * (frame.time / period) + object.shadowOpacity()
      );

      if (object.shadowOpacity() <= 0) {
        object.shadowOpacity(0);
        object.shadowBlur(0);
        object.shadowEnabled(false);
        anim.stop();
        callback();
      }
    }, object.getStage().children[0]);
    anim.start();
  }

  render() {
    return (
      <Image
        draggable
        name={this.props.src}
        width={this.props.width}
        height={this.props.height}
        x={this.state.x}
        y={this.state.y}
        image={this.state.image}
        ref={node => {
          this.imageNode = node;
        }}
        onContextMenu={e => {
          new ContextMenu(e, this.state.fragment).handleContextMenu();
        }}
        onTransform={this.handleTransform}
        shadowColor="white"
        shadowBlur={10}
        shadowOpacity={0.9}
        opacity={this.props.opacity}
        scaleX={this.props.scaleX}
      />
    );
  }
}
