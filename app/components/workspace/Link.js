export default class Link {
    constructor(obj1, obj2, layer) {
        this.obj1 = obj1;
        this.obj2 = obj2;
        this.layer = layer;

        this.line = new Konva.Line({
            points: [this.obj1.x(), this.obj1.y(), this.obj2.x(), this.obj2.y()],
            stroke: '#bdc3c7',
            strokeWidth: 2,
            lineCap: 'round',
            lineJoin: 'round',
        });

        obj1.on('dragmove', () => this.updateLine());
        obj2.on('dragmove', () => this.updateLine());
    }
 
    updateLine() {
        this.line.points([this.obj1.x(), this.obj1.y(), this.obj2.x(), this.obj2.y()]);

        if(!this.layer.children.includes(this.obj1) || !this.layer.children.includes(this.obj2)) {
            this.line.destroy();
        }

        this.layer.batchDraw();
      }
}