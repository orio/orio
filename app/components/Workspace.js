import React from 'react';
import { Stage, Layer, Transformer } from 'react-konva';
import Handler from './workspace/Handler.js';
import Fragment from './workspace/Fragment';
import styles from './styles/Workspace.css';
import Explorer from './Explorer';
import { white } from 'color-name';



export default class Workspace extends React.Component {

  state = {
    stage: null,
    layer: null,
    selectedShapeName: '',
    localFragments: [],
    images: this.props.fragmentsList,
    stageWidthRatio: null,
  };
  

  componentDidMount() {
    this.setState({ stage: this.stageRef.getStage(),
                    layer: this.layerRef.getLayer() });
    // this.changeExplorerState = this.changeExplorerState.bind(this);
    this.state.stageWidthRatio = this.props.explorerState.availableWindowWidth;
    // console.log('STAGE')
    // console.log(this.state.stage.width)

    // console.log("PROPS DID MOUNT")
    // console.log(this.props)
  }

  handleStageClick = e => {
    this.setState({ selectedShapeName: e.target.name() });
    console.log("Recto click");
    console.log(e.target.getStage().getPointerPosition());
  };


  handleWheel = e => {
    e.evt.preventDefault();

    const scaleBy = 1.05;
    const stage = e.target.getStage();
    const oldScale = stage.scaleX();

    const mousePointTo = {
      x: stage.getPointerPosition().x / oldScale - stage.x() / oldScale,
      y: stage.getPointerPosition().y / oldScale - stage.y() / oldScale
    };

    let newScale = e.evt.deltaY > 0 ? oldScale * scaleBy : oldScale / scaleBy;

    newScale = newScale < 0.15 ? 0.15 : newScale;

    stage.scale({ x: newScale, y: newScale });
    this.setState({
      stageScale: newScale,
      stageX:
        -(mousePointTo.x - stage.getPointerPosition().x / newScale) * newScale,
      stageY:
        -(mousePointTo.y - stage.getPointerPosition().y / newScale) * newScale
    });
    var newPos = {
      x: -(mousePointTo.x - stage.getPointerPosition().x / newScale) * newScale,
      y: -(mousePointTo.y - stage.getPointerPosition().y / newScale) * newScale
    };

    stage.position(newPos);
    stage.batchDraw();
    // console.log()
    this.props.synchronizePosition({
      x: stage.absolutePosition().x,
      y: stage.absolutePosition().y,
      scaleX: stage.scaleX(),
      scaleY: stage.scaleY(),
      fragmentsVerso: this.state.localFragments
    });
    this.forceUpdate();
  };

  updateStage = e => {

    this.state.localFragments = [];
    let images = this.state.layer.getChildren(function(node){
      return node.getClassName() === 'Image';
   });

   images.forEach(image => {
    let fragment = {
      src: image.attrs.name,
      position: {x: image.attrs.x, y: image.attrs.y},
      rotation: image.attrs.rotation,
      id: image.id,

    }
    this.state.localFragments.push(fragment);
    // console.log("LASTPOS")
    // console.log(image)
   });





    e.evt.preventDefault();
    const stage = e.target.getStage();



    this.props.synchronizePosition({
      x: stage.absolutePosition().x,
      y: stage.absolutePosition().y,
      scaleX: stage.scaleX(),
      scaleY: stage.scaleY(),
      fragmentsVerso: this.state.localFragments,
    });

    this.forceUpdate();

  }

  addFragment = e => {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(e.target.value);
    }
  };

  onDrop = e => {


    let fragment = {
      src: e.dataTransfer.getData('src'),
      position: this.getStageRelativePointerPosition(e),
      id: e.dataTransfer.getData('id')
    }
    this.props.addFragment(fragment);

    this.state.localFragments.push(fragment);
    this.hideExplorerThumbnail(e.dataTransfer.getData('id'));

    this.forceUpdate();
  };

  removeFragment = fragment => {
    this.props.removeFragment(fragment);
    this.showExplorerThumbnail(fragment.props.id);
    this.forceUpdate();
  };

  displayHints = listHints => {
    this.props.displayHints(listHints);
  }

  getStageRelativePointerPosition(e) {
    let stage = this.state.stage;
    let layer = stage.children[0];
    // the function will return pointer position relative to the passed node

    // // to detect relative position we need to invert transform
    // transform.invert();

    // get pointer (say mouse or touch) position
    stage.setPointersPositions(e);
    let pos2 = {
      x: (stage.getPointerPosition().x - stage.getAbsolutePosition().x) / stage.scaleX(),
      y: (stage.getPointerPosition().y - stage.getAbsolutePosition().y) / stage.scaleY()
    };

    // now we find relative point
    return pos2;
  }

  getStagePointerPosition(e, stage = this.state.stage) {
    stage.setPointersPositions(e);
    let pos = {
      x: stage.getPointerPosition().x - stage.getAbsolutePosition().x,
      y: stage.getPointerPosition().y - stage.getAbsolutePosition().y
    };
    return pos;
  }

  hideExplorerThumbnail(id) {
    let thumbnail = document.getElementById(id);
    thumbnail.style.opacity = 0.15;
  }

  showExplorerThumbnail(id) {
    let thumbnail = document.getElementById('thumbnail|' + id.split('|')[1]);
    console.log(thumbnail);
    if(thumbnail != null) {
      thumbnail.style.opacity = 1;
    }
  }

  

  componentDidUpdate() {
    console.log("RECTO updated")
    // if(this.state.stageWidthRatio != this.props.explorerState.availableWindowWidth) {
    // console.log("workspace updated")
      
    //   this.state.stageWidthRatio = this.props.explorerState.availableWindowWidth
    //   this.state.stage.width((window.innerWidth * this.state.stageWidthRatio)/2)
    //   console.log(this.state.stage)
    //   // if(this.state.stageWidthRatio == 1) {
    //   //   document.getElementById('explorer').className = styles.explorerWrapped;
    //   //   document.getElementById('btnWrapExplorer').className = styles.btnExplorerWrapped;
    //   //   // console.log("Now wrapped")
    //   // } else if(document.getElementById('explorer').className == styles.explorerWrapped){
    //   //   document.getElementById('explorer').className = styles.explorerExtended;
    //   //   document.getElementById('btnWrapExplorer').className = styles.btnExplorerExtended;
    //   //   // console.log("Now extended")
    //   // } 
    // }
    
  }
 


  render() {

    // const {
    //   stagePositions
    // } = this.props;

    const availableWindowWidth = this.props.explorerState.availableWindowWidth;

    return (
      <div className={styles.backgroundWhite} id={"background"}>
        <div className={styles.buttons}>
          <button onClick={() => document.getElementById('background').className = styles.backgroundWhite}></button>
          <button onClick={() => document.getElementById('background').className = styles.backgroundBlack}></button>

        </div>
      
        <div
          className={styles.content}
          onDrop={this.onDrop}
          onDragOver={e => e.preventDefault()}
        >
          
          <Stage
            ref={ref => {
              this.stageRef = ref;
            }}
            name="stage"
            draggable
            // className={styles.stageWrapped}
            onWheel={this.handleWheel}
            onMouseUp={this.updateStage}
            scaleX={this.state.stageScale}
            onClick={this.handleStageClick}
            scaleY={this.state.stageScale}
            width={(window.innerWidth * availableWindowWidth)/2}
            height={window.innerHeight}
          >
            <Layer ref={ref => {
              this.layerRef = ref;
            }}
            name="layer">
              {this.state.images.map((image, index) => {
                  return (
                    <Fragment
                      key={'fragment|' + image.id.split('|')[1]}
                      id={'fragment|' + image.id.split('|')[1]}
                      className={styles.popshadow}
                      src={image.src}
                      position={image.position}
                      removeFragment={this.removeFragment}
                      displayHints={this.displayHints}
                    />
                  );
                  this.state.localFragments.push(image);
                }
              )}
              <Handler selectedShapeName={this.state.selectedShapeName} />
            </Layer>
          </Stage>
        </div>
      </div>
    );
  }
}
