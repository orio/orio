import React from 'react';

import {
  MagnifierContainer,
  MagnifierPreview,
  MagnifierZoom
} from 'react-image-magnifiers';

import styles from './styles/Thumbnail.css';

export default class Thumbnail extends React.Component {
  constructor(props) {
    super(props);
    this.myRef = React.createRef();
  }

  showGlass(e) {
    e.target.style.display = 'none';
    e.target.nextSibling.style.position = 'static';
    e.target.nextSibling.style.visibility = 'visible';
    document.getElementById(this.props.element).firstChild.style.left = '25vw';
  }

  hideGlass() {
    let id = 'thumbnail|' + this.props.element;

    document.getElementById(id).style.display = 'block';
    document.getElementById(id).nextSibling.style.visibility = 'hidden';
    document.getElementById(id).nextSibling.style.position = 'absolute';
    document.getElementById(this.props.element).firstChild.style.left =
      '-1000px';
  }

  isDraggable(url) {
    let draggable = true;
    this.props.images.forEach(element => {
      let arraySrc = element.src.split('/');
      let src = arraySrc[arraySrc.length - 1];
      if (src === url) {
        draggable = false;
      }
    });

    return draggable;
  }

  setOpacityDynamic() {
    let url = this.props.element.split('/')[1];
    console.log(url);
    let image = document.getElementById('thumbnail|' + this.props.element);
    if (image) {
      if (this.isDraggable(url)) {
        image.style.opacity = 1;
        return;
      }
      image.style.opacity = 0.15;
    }
  }

  componentDidMount() {
    this.setOpacityDynamic();
  }

  render() {
    return (
      <div
        ref={this.myRef}
        className={styles.layer}
        onDragStart={e => {
          let url = 'img/frag/' + this.props.element;
          if (this.isDraggable(url)) {
            e.dataTransfer.setData('src', url);
            e.dataTransfer.setData(
              'id',
              this.myRef.current.firstChild.getAttribute('id')
            );
          } else {
            e.preventDefault();
          }
        }}
      >
        <img
          className={styles.fitImage}
          id={'thumbnail|' + this.props.element}
          src={process.cwd() + '/resources/images/' + this.props.element}
          onClick={e => this.showGlass(e)}
          width={window.innerWidth * 0.11}
        />
        <MagnifierContainer
          style={{ visibility: 'hidden', position: 'absolute' }}
        >
          <div
            onMouseOut={() => this.hideGlass()}
            className={styles.previewContainer}
          >
            <MagnifierPreview
              imageSrc={
                'img/frag/' + this.props.element
              }
              className={styles.preview}
            ></MagnifierPreview>
          </div>
          <div id={this.props.element}>
            <MagnifierZoom
              className={styles.zoomViewer}
              imageSrc={
                'img/frag/' + this.props.element
              }
            />
          </div>
          <div className={styles.infos}>
            <p>{this.props.element}</p>
          </div>
        </MagnifierContainer>
      </div>
    );
  }
}
