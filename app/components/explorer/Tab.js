import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './styles/Tab.css';

export default class Tab extends Component {
  static propTypes = {
    activeTab: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
  };

  onClick = () => {
    const { label, onClick } = this.props;
    onClick(label);
  };

  render() {
    const {
      onClick,
      props: { activeTab, label }
    } = this;
    return (
        <li id={"tab_"+label} className={activeTab === label ? styles.tabListActive : ""} onClick={onClick}>
            {label}
        </li>
    );
  }
}
