// @flow
// import { combineReducers } from 'redux'
import {
  ADD_FRAGMENTS,
  REMOVE_FRAGMENTS,
  DISPLAY_HINTS,
  SYNC_POSITION,
  SYNC_FRAGMENTS,
  CHANGE_TAB
} from '../actions/fragments';

// import {
//   WRAP_EXPLORER,
//   EXTEND_EXPLORER
// } from '../actions/explorer';

import type { Action } from './types';

export default function fragments(
  state: Object = {
    fragmentsRecto: [],
    fragmentsVerso: [],
    fragmentsHints: [],
    stagePosition: { x: 0, y: 0, scaleX: 1, scaleY: 1 }
  },
  action: Action
) {
  switch (action.type) {
    case ADD_FRAGMENTS:
      state.fragmentsRecto.push(action.fragment);
      state.fragmentsVerso.push(action.fragment);
      return state;

    case REMOVE_FRAGMENTS:
      var target = state.fragmentsRecto.findIndex(i => i.id === action.id);
      state.fragmentsRecto.splice(target, 1);
      state.fragmentsVerso.splice(target, 1);
      return state;

    case DISPLAY_HINTS:
      state.fragmentsHints = action.hintsList;
      return state;

    case SYNC_POSITION:
      state.stagePosition = action.stagePositions;
      state.fragmentMoving = action.fragmentMoving;
        state.fragmentsVerso = action.fragmentsVerso;
      return state;
    default:
      return state;
  }
}

// export default function explorer(
//   state: Object = {
//     isExplorerExtended: true,
//     availableWindowWidth: 0.78,
//   },
//   action: Action
// ) {
//   switch (action.type) {
//     case WRAP_EXPLORER:
//       state.isExplorerExtended = action.isExplorerExtended;
//       state.availableWindowWidth = action.availableWindowWidth;
//       return state;

//     case EXTEND_EXPLORER:
//       state.isExplorerExtended = true;
//       state.availableWindowWidth = 0.78;
//       return state;

//     default:
//       return state;
//   }
// }

// const todoApp = combineReducers({
//   fragments,
//   explorer
// })
// export default todoApp
