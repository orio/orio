// @flow
// import { combineReducers } from 'redux'
  import {
    WRAP_EXPLORER,
    EXTEND_EXPLORER
  } from '../actions/explorer';
  
  import type { Action } from './types';
  

  
  export default function explorer(
    state: Object = {
      isExplorerExtended: true,
      availableWindowWidth: 0.78,
    },
    action: Action
  ) {
    switch (action.type) {
      case WRAP_EXPLORER:
        state.isExplorerExtended = action.isExplorerExtended;
        state.availableWindowWidth = action.availableWindowWidth;
        return state;
  
      case EXTEND_EXPLORER:
        state.isExplorerExtended = action.isExplorerExtended;
        state.availableWindowWidth = action.availableWindowWidth;
        return state;
  
      default:
        return state;
    }
  }
  
 
  