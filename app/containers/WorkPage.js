import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Work from '../components/Work';
import * as FragmentsActions from '../actions/fragments';
import * as ExplorerActions from '../actions/explorer';


function mapStateToProps(state) {
  return {
    mainState: state.mainState,
    explorerState: state.explorer
  };
}
  
function mapDispatchToProps(dispatch) {
  console.log("REDUX")
  console.log(FragmentsActions)

  return {
    fragmentsActions: bindActionCreators(FragmentsActions, dispatch),
    explorerActions: bindActionCreators(ExplorerActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Work);
