// @flow
import type { GetState, Dispatch } from '../reducers/types';

export const WRAP_EXPLORER = 'WRAP_EXPLORER';
export const EXTEND_EXPLORER = 'EXTEND_EXPLORER';


export function wrapExplorer(data) {
  return {
    isExplorerExtended: data.isExplorerExtended,
    availableWindowWidth: data.availableWindowWidth,
    type: WRAP_EXPLORER
  };
}

export function extendExplorer(data) {
  return {
    isExplorerExtended: data.isExplorerExtended,
    availableWindowWidth: data.availableWindowWidth,
    type: EXTEND_EXPLORER
  };
}

