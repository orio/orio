// @flow
import type { GetState, Dispatch } from '../reducers/types';

export const ADD_FRAGMENTS = 'ADD_FRAGMENTS';
export const REMOVE_FRAGMENTS = 'REMOVE_FRAGMENTS';
export const DISPLAY_HINTS = 'DISPLAY_HINTS';
export const SYNC_POSITION = "SYNC_POSITION";
export const SYNC_FRAGMENTS = "SYNC_FRAGMENTS";
export const CHANGE_TAB = "CHANGE_TAB";


export function addFragment(image) {
  return {
    fragment: image,
    type: ADD_FRAGMENTS
  };
}

export function changeTab(tab) {
  return {
    tab: tab,
    type: CHANGE_TAB
  };
}

export function removeFragment(fragment) {
  return {
    id: "thumbnail|"+fragment.props.id.split("|")[1],
    type: REMOVE_FRAGMENTS
  };
}

export function displayHints(hintsList) {
  return {
    hintsList: hintsList,
    type: DISPLAY_HINTS
  };
}
export function synchronizePosition(positions) {
  return {
stagePositions: {x: positions.x, y: positions.y, scaleX: positions.scaleX, scaleY: positions.scaleY} ,
    fragmentsVerso: positions.fragmentsVerso, //A completer
    type: SYNC_POSITION
  };
}
