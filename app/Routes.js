import React from 'react';
import { Switch, Route } from 'react-router';
import routes from './constants/routes.json';
import App from './containers/App';
import HomePage from './containers/HomePage';
import WorkPage from './containers/WorkPage';

export default () => (
  <App>
    <Switch>
      <Route path={routes.WORKPAGE} component={WorkPage} />
    
      <Route path={routes.HOME} component={HomePage} />
    </Switch>
  </App>
);
