/!\ The most up to date branch is the branch "taho" /!\
read file "livraison_taho_et_revue_de_code.md"
# orio_project

Orio is a tool that will be able to enhance papyrologists' work by offering an intuitive workspace.

Twitter :
- @OrioDawin

Authors :
- AUBERT Daphné
- BALFOUONG Florentin
- LE FOLLEZOU BAR Léo
- ROOSEBROUCK Benjamin
- ROSSIGNOL Lucas

# Get Started

- git clone https://gitlab-ce.iut.u-bordeaux.fr/fbalfouong/orio_project.git
- npm install
- yarn dev
