# Revue de code Taho

- Pas mal d'images, quand on les drag sur le workspace, il se passe rien, exemples : 2856e, 2867f, 2867b, 2866
- Dans app/components/Explorer.js filters ligne 29
- yarn dev pour lancer en mode dev
- packager le projet : yarn package
  - ça crée une version linux et une version windows
  - ATTENTION ça a l'air de packager la branche master ..
- app/components/taskbar.js -> ligne 145 pour l'affichage du menu de filtres
- problèmes avec les images segmentées (fond noir), et certains ont un fond blanc ?? (ça vient de moi ça, Antoine)
- clic-droit -> show possibilities fait tout planter
- Taho a touché à ces fichiers : (git diff --name-only 35b7c073 )
  - app/components/Explorer.js
  - app/components/Home.js
  - app/components/Taskbar.js
  - app/components/Work.js
  - app/components/Workspace.js
  - app/components/WorkspaceVerso.js
  - app/components/explorer/Thumbnail.js
  - app/components/tagmenu/Choice.js
  - app/components/tagmenu/SubMenu.js
  - app/components/tagmenu/TagMenu.js
  - app/components/tagmenu/styles/Menu.css

Question : y'avait un bug quand on quittait mal le logiciel, une histoire de sauvegarde

- Probablement que lors de la sauvegarde, le fichier de sauvegarde .json est vidé puis ré écrit. Si on quitte avant qu'il soit ré-écrit, le fichier est vide et on a perdu les infos du projet
- Il faudrait catch l'action de quitter l'app et faire une sauvegarde propre avant





